(ns laclj.core
  (require [clojure.edn :refr read-string]
           [clojure.string :as s])
  (:gen-class))



(comment ;this is what command dose
(println ((command :#hi) "[utf8]" "content"))
(println ((command :hi)  "content")))

(defn command 
  [clj]
  (let [command (name clj) 
        env? (= \# (first command))
        command (if env? 
                  (apply str (drop 1 command)) 
                  command)]
  (if env?
    (fn [args content]
      (str "\\begin"
           args
           "{" command "}\n"
           content
           "\n\\end{" command "}\n"
           ))
    (fn [args content]
         (str "\\" command args "{" content "}" )))))

(comment ;This is what argv dose
(argv ["10pt" "a4paper"]))

(defn argv 
  ([{:as kwargs}]
   (print kwargs))
  ([[args]]
  (format "[%s]" (s/join \, args))))

  


; this is suposet to dispatch to allow for parsing
(defmulti laclj (fn [& args] (mapv class args)))
(defmethod laclj [clojure.lang.Keyword ] [& args] 
  (apply command args))

(laclj :hi nil nil)

(class :hi)

; ###############################
; command line UI
(defn -main
  [IF & args]

  (let [output 
        (if (empty? args)  
          (fn [stuff] (print stuff))
          (partial slurp (first args)))]

    (->> IF
         slurp
         read-string
         laclj
         output)))


(comment "example format"
(def clj (read-string (slurp "test/basic.texclj"))))
