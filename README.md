# LaClj
LaClj is aims compiles edn to LaTex.
Why?
* LaTex has bad syntax.
* LaTex has bad macros.
* LaTex is not compostable.

LaClj is very much a work in progress.

## sample syntax

```clojure
[[:documentclass ["10pt" "a4paper"] "article" ]
 [:usepackage ["utf8"] "inputenc" ]
 [:usepackage  "amsmath" ]
 [:usepackage {:left "2cm" 
               :right "2cm" 
               :top "2cm" 
               :bottom "2cm"} "geometry" ]

 [:author "Maxwell Gisborne"]
 [:title ]
 [:#document 
  "stuff gose hear"
  ]]

```
